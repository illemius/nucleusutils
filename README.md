# NucleusUtils
Collection of utils made by Illemius/Alex Root Junior (c) 2016-2017

## Setup
### From PyPi
1. Install: `pip install NucleusUtils`
1. ?!?
1. Profit!

### From Git (Bitbucket)
1. Clone: `git clone https://bitbucket.org/illemius/nucleusutils.git`
1. Go to dir: `cd NucleusUtils`
1. Install: `python setup.py install`

## Usage
Read [docs](htpps://bitbucket.org/illemius/nucleusutils/wiki/Home)
