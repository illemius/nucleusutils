#!/usr/bin/env bash

function build {
    python setup.py sdist bdist_wheel
}

function rebuild {
    rm -rf dist build NucleusUtils.egg-info
    build
}

function upload {
    twine upload dist/*
}

function quit {
    exit
}

case $1 in
    build)
        build
    ;;
    rebuild)
        rebuild
    ;;
    upload)
        upload
    ;;
    *)
        echo "Usage: '$0 build|rebuild'"
        quit
    ;;
esac

