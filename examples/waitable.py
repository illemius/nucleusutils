import threading

from NucleusUtils.simple.dict import WaitableDict

data = WaitableDict()


def fill():
    for index in range(10):
        data[index] = index


def getter():
    for index in range(9, 0, -1):
        threading.Thread(target=print, args=(data[index],), name='wait ' + str(index)).start()


if __name__ == '__main__':
    threading.Thread(target=getter, name='getter').start()
    threading.Thread(target=fill, name='fill').start()
