import random

from NucleusUtils.events import Event
from NucleusUtils.events.attr import EventAttr, SpecificEventAttr


class MyClass:
    attr = EventAttr()

    on_change_foobar = Event(name='Change foobar', threaded=False)
    foobar = SpecificEventAttr(lambda value: value in range(5), on_change_foobar)

    def __init__(self):
        self.attr = None
        self.foobar = None


my = MyClass()

# my.attr__changed.subscribe(lambda value: print('my.attr is changed:', value))
my.attr__changed += lambda value: print('my.attr is changed:', value)
my.on_change_foobar += lambda value: print('foobar in range(5):', value)

for val in range(5):
    my.attr = val
    my.foobar = random.randint(0, 10)

print('my.attr__changed.triggered', my.attr__changed.triggered)
print('my.on_change_foobar.triggered', my.on_change_foobar.triggered)
