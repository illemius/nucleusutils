import os
import sys

from NucleusUtils.i18n.locales import load_from_path, context_processor
from NucleusUtils.i18n._parser import add_parser, NumEndingsParser, ConstantParser, SimpleFormatParser
from NucleusUtils.i18n.translator import translate as _, set_default_locale

PATH = os.path.dirname(os.path.realpath(sys.argv[0]))

# Load locales
load_from_path(os.path.join(PATH, 'locales'))
print('Locales:', ', '.join(context_processor.get_locales()), end='\n\n')
set_default_locale('ru_RU')

# Register parsers
add_parser(NumEndingsParser())
add_parser(ConstantParser({'FOO': 'bar'}))
add_parser(SimpleFormatParser())

# Test ConstantParser and SimpleFormatParser
print(_('test', parse=False))
print(_('test', env={'test': 'foo'}))

print()

# Test NumEndingsParser
print(_('track', parse=False))
for num in range(6):
    print(_('track', env={'tracks_count': num}))

"""
Locales: en_US, ru_RU

окей {test} {FOO} баз
окей foo bar баз

В твоей истории {tracks_count:нет трэков|# трэк|# трэка|# трэков}
В твоей истории нет трэков
В твоей истории 1 трэк
В твоей истории 2 трэка
В твоей истории 3 трэка
В твоей истории 4 трэка
В твоей истории 5 трэков
"""
