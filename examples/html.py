from NucleusUtils.web.html import Html, Head, Body, Tag, Comment, A, Img, Div

document = Html([
    Head(),
    Body([
        Tag('ul', content=[
            Tag('li', content=['Test 1']),
            Tag('li', content=['Test 2'])
        ]),
        Comment('Hello world!'),
        A('Lorem ipsum', href='#', tag_class=['btn', 'btn-info'], style={'color': '#fff', 'background': '#000'}),
        Img(src='/media/blog/2016/12/15/Dfw3BdaasCl.png', alt='FooBar'),
        Div(tag_class='row', content=[
            Div(tag_class=['col-md-4'], content=['Foo']),
            Div(tag_class=['col-md-4'], content=['Bar']),
            Tag('div', tag_class=['col-md-4'], content=['Baz']),
        ])
    ]),
])

print(document)
print(document.select('.btn').get_content())
print(list(document.select_all('.col-md-4')))
print([element.get_content() for element in document.select_all('li')])
