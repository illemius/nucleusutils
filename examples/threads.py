import threading
import time

from NucleusUtils.threads import Thread


@Thread
def test(index, __thread__: threading.Thread):
    print('start:', index, __thread__.getName())
    time.sleep(1)
    print('finish:', index, __thread__.getName())


for item in range(5):
    test(item)
