import os
import sys

from NucleusUtils.logging.decorators import handle_exceptions
from NucleusUtils.logging.dumps import set_dumps_path

PATH = os.path.dirname(os.path.realpath(sys.argv[0]))
set_dumps_path(os.path.join(PATH, 'dumps'))


def handler(func, args, kwargs, dump):
    print('Exception:', dump.file)


@handle_exceptions(handler=handler)
def foobar(foo='hello', bar='world'):
    print(foo, bar)


foobar()
