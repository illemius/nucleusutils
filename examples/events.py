from NucleusUtils.events import Event, event_trigger, event_handler

# Register events
foo_event = Event('foo')
bar_event = Event('bar')


# You can use decorator for triggering event when function is called
@event_trigger(foo_event)
def foo_trigger(text):
    # Do something here
    return text


# And you can use decorator for subscribing for event. But it's impossible when use class methods!
@event_handler(foo_event)
def foo_handler(result, *args, **kwargs):
    print('Handle:', foo_event, '| Result:', result)


# Simple event handler
def bar_handler(result):
    print('Handle:', bar_event, '| Result:', result)


# Register (subscribe) handlers
# You can use any callable object
bar_event += bar_handler
bar_event += lambda text: print('Lambda handler.', text)

# Trigger events
# using decorated function
foo_trigger('Hello world!')
# or use __call__() method. (or alias .trigger())
bar_event('Lorem ipsum dolor sit amet')
