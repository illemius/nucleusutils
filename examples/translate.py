import os
import sys

from NucleusUtils.i18n.locales import load_from_path, context_processor
from NucleusUtils.i18n.translator import set_default_locale, translate, jinja_env, LazyTranslate

PATH = os.path.dirname(os.path.realpath(sys.argv[0]))

# Load locales
load_from_path(os.path.join(PATH, 'locales_2'))
print('Locales:', ', '.join(context_processor.get_locales()), end='\n\n')
set_default_locale('test_2')


def test(value):
    print(value)
    return value.upper()

jinja_env.filters['test'] = test

print(translate('test', env={}))
print(translate('foo', locale='test'))

print()
text = LazyTranslate('demo')
print(text)
print(translate(text))

for i in range(10):
    print(translate('foo', env={'num': i}))
