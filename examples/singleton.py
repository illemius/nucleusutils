from NucleusUtils.singleton import SingletonMetaClass


class Test(metaclass=SingletonMetaClass):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return self.name


foo = Test('foo')
bar = Test('bar')

print(foo)
print(bar)
