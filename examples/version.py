from NucleusUtils.versions import Version

VERSION = (1, 4, 7, 'b')
__version__ = Version(VERSION).version


def example():
    print('Version:', __version__)


if __name__ == '__main__':
    example()
