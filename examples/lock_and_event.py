import time

from NucleusUtils.events.lock import Lock


class FooBar:
    def __init__(self):
        self.lock = Lock()
        self.counter = 0

    def fire(self):
        # Check lock
        self.lock.check()

        # Lock resource
        self.lock.acquire()
        print('FIRE')

        # Do something here. For e. g. sleep 1 second
        time.sleep(1)

        # Unlock resource
        self.lock.release()

    def inc(self):
        self.counter += 1

    def kaboom(self):
        if self.counter >= 2:
            # Try to execute `fire` with locked Lock
            self.fire()

            # Recommend to use safe threading method `Lock.do`
            # self.lock.do(self.fire)


if __name__ == '__main__':
    # Create object instance
    foo = FooBar()

    # Subscribe for event
    foo.lock.on_change += lambda status: print('Resource is', 'locked' if status else 'unlocked')
    foo.lock.on_release += foo.inc
    foo.lock.on_acquire += foo.kaboom

    # Call foo.fire() four times
    for _ in range(3):
        foo.fire()

