from NucleusUtils.versions.deprecated import Deprecated, simple_deprecated


@Deprecated('Test')
def deprecated_method():
    print('Foo bar')


@simple_deprecated('Test')
def simple_deprecated_method():
    print('Test')


def example():
    deprecated_method()
    simple_deprecated_method()


if __name__ == '__main__':
    example()
