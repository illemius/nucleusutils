import logging
import os
import sys

from NucleusUtils.config import Config

logging.basicConfig(level=logging.DEBUG)
PATH = os.path.dirname(os.path.realpath(sys.argv[0]))
CONFIGS_PATH = os.path.join(PATH, 'configs')

json_config = Config(os.path.join(CONFIGS_PATH, 'example.json')).load()
xml_config = Config(os.path.join(CONFIGS_PATH, 'example.xml')).load()

print('from JSON:\t', json_config.as_dict())
print('from XML:\t', xml_config.as_dict())

print(xml_config['foo'], json_config['bar'], json_config['baz'])
print('is an JSON equal XML? -', xml_config.as_dict() == json_config.as_dict())
