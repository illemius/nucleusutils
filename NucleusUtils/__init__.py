from . import versions

VERSION = versions.Version(0, 1, 11, 'beta')
__version__ = VERSION.version
